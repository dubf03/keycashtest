@extends('layouts.app')

@section('content')
    <div class="row">
        <h2 class="text-center">
            Lista de imóveis
        </h2>
    </div>

    <div class="row">
        <ul class="list-group">
            @foreach ($list as $realty)
                <li class="list-group-item">
                    <div class="card">
                        <div class="gallery col-6">
                            @foreach ($realty->photos as $photo)
                                <span class="image">
                                    <img src="{{$photo->path}}">
                                </span>
                            @endforeach
                        </div>
                        <div class="card-body col-6">
                            <h5 class="card-title">Imóvel à venda</h5>
                            <p class="card-text">{{$realty->description}}</p>
                            <p class="card-text">{{$realty->address}}</p>
                            <table class="table">
                                <tr>
                                    <td><span class="col-6">R$ {{$realty->price}}</span></td>
                                    <td><span class="col-6">{{$realty->area}} m2</span></td>
                                </tr>
                                <tr>
                                    <td><span class="col-6">Quartos: {{$realty->bedrooms}}</span></td>
                                    <td><span class="col-6">Suítes: {{$realty->suites}}</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection