@extends('layouts.app')

@section('content')
@if (isset($success))
        <p class="alert alert-success">{{$success}}</p>
    @endif
    @if (isset($danger))
        <p class="alert alert-danger">{{$danger}}</p>
    @endif
    <div class="row">
        <h2 class="text-center">
            Cadastro de imóvel
        </h2>
    </div>

    <section class="row">
        <form action="/" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table">
                <tr class="form-group">
                    <td>
                        <label>Nome: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="name" id="name"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>CPF: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="cpf" id="cpf" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Profissão: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="occupation" id="occupation" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Endereço do imóvel: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="address" id="address" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Valor do imóvel: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="price" id="price" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Área útil: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="area" id="area" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label>Quantidade de quartos: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="bedrooms" id="bedrooms" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label>Quantidade de suítes: </label>
                    </td>
                    <td>
                        <input class="form-control" required type="text" name="suites" id="suites" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label>Descrição: </label>
                    </td>
                    <td>
                        <textarea class="form-control" required name="description" id="description"> </textarea>
                    </td>
                </tr>
            </table>

            <h2>
                Imagens
            </h2>

            <table class="table">
            <tr>
                    <td>
                        <label>Fotos do imóvel: </label>
                    </td>
                    <td>
                        <input required class="form-control" type="file" name="img[]" id="img" multiple/>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input class="form-control btn btn-primary" type="submit" id="submit" value="Cadastrar"/>
                    </td>
                </tr>
            </table>

        </form>
    </section:>
@endsection