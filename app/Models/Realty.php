<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Realty extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'cpf', 'occupation', 'address', 'price', 'area', 'bedrooms', 'suites', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function photos()
    {
        return $this->hasMany(Gallery::Class);
    }
}
