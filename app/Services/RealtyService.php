<?php

namespace App\Services;
use App\Models\Realty;
use App\Services\GalleryService;

class RealtyService {

    protected $model;
    protected $gallery;

    public function __construct(Realty $model, GalleryService $gallery){
        $this->model = $model;
        $this->gallery = $gallery;
    }

    public function store($data){
        $realty = $this->model->create($data);
        if( $realty ){
            foreach($data['img'] as $file) {
                $this->gallery->store($realty->id, $file);
            }
            return true;
        }else {
            return false;
        }
    }

    public function getAll(){
        return $this->model->all();
    }

}