<?php

namespace App\Services;
use App\Models\Gallery;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class GalleryService {

    protected $model;
    protected $storage;

    public function __construct(Gallery $model, Storage $storage){
        $this->model = $model;
        $this->storage = $storage;
    }

    public function store($realty_id, $file) {
        $path = str_random(1).'/'.str_random(1).'/'.str_random(1).'/'.str_random(30).'.jpg';
        $image = Image::make($file)->fit(300,300)->encode('jpg', 50);
        $this->storage->disk('local')->put('public/'.$path, $image);
        $data = [
            'path' => 'storage/'.$path,
            'realty_id' => $realty_id
        ];
        $this->model->create($data);
    }

}