<?php

namespace App\Http\Controllers;
use App\Services\RealtyService;
use Illuminate\Http\Request;

class RealtyController{

    protected $service;

    public function __construct(RealtyService $service){
        $this->service = $service;
    }

    public function formRegister() {
        return view('register');  
    }

    public function store(Request $request) {
        $realty = $request->all();
        if($this->service->store($realty)){
            $data = [
                'success' => 'Cadastro efetuado com sucesso!'
            ];
        }else {
            $data = [
                'danger' => 'Ocorreu um erro ao cadastrar o imóvel!'
            ];
        }

        return view('register', $data);  
    }

    public function list() {
        $data = ['list' => $this->service->getAll()];
        return view('list', $data);
    }

}